<?php

use fisherr\news\Module;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel fisherr\news\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Module::t('fisherr-news', 'News');
$this->params['subtitle'] = Module::t('fisherr-news', 'List News');
$this->params['breadcrumbs'][] = [
    'label' => $this->title,
    'url' => ['index']
];
$this->params['breadcrumbs'][] = $this->params['subtitle'];

$gridId = 'news-grid';

?>
<div class="<?= $gridId ?>">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1><?= $this->title ?></h1>
        </div>
    </div>

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' => '_item'
    ]) ?>
</div>
